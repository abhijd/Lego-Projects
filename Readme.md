# Lego-Projects

This repository contains the code for two different lego projects. The projects are:

* PICK AND PLACE ROBOT / Brick Sorter Robot
* Line Follower Robot


_**Please open the Readme.pdf file to see further instructions about both the projects*_*


### How to make the robots

* Assemble the robot using the assembly instructions in Lego websites. A simple google search will bring many options. Choose the one that is similar to the one shown in the readme.pdf
* After assembling, load the appropriate code (the file with .nxc extensions) into the brick.
* Use the control in the brick to browse to the loaded program and run it. Voila! 


## Acknowledgments

* Special thanks to Lecturer [Andrei Lobov](mailto:andrei.lobov@tut.fi) for his excellent lectures and guidance during the Introduction to Automation, and [Luis Gonzalez Moctezuma](mailto:luis.gonzalezmoctezuma@tut.fi)  for excellent guidance with the line follower Robot.
* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required for the project. 